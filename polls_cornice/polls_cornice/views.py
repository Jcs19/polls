""" Cornice services.
"""
from cornice import Service


hello = Service(name='hello', path='/', description="Simplest app")
code = Service(name='code', path='/code', description="kawabongaaaaaaaa")

@hello.get()
def get_info(request):
    """Returns Hello in JSON."""
    return {'Hello': 'World'}
    
@hello.post()
def post_hello(request):
    print(request.POST['ddd'])
    

@code.get()
def get_code_secret(request):
    return {'code': 'A5G FD4 F9G DF9 9BG'}
